package com.example.kjankiewicz.android_16w01_mymaps;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class GoogleMapActivity extends ActionBarActivity
        implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    boolean mMapReady = false;
    GoogleMap mGoogleMap;

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng poznan = new LatLng(52.408513, 16.934110);
        Marker poznanMarker = map.addMarker(new MarkerOptions()
                .position(poznan)
                .title("Poznań"));
        //poznanMarker.showInfoWindow();

        LatLng cwPut = new LatLng(52.404220, 16.949610);
        MarkerOptions cwPutMarkerOptions = new MarkerOptions()
                .position(cwPut)
                .alpha((float) 0.5)
                .draggable(true)
                .visible(true)
                .rotation(50)
                .flat(true)
                .anchor((float) 0.5, (float) 0.5)
                .snippet("Centrum Wykładowe")
                .title("Politechnika Poznańska");
        map.addMarker(cwPutMarkerOptions);

        /*map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.remove();
                return true;
            }
        });*/

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            private final View contents = getLayoutInflater().inflate(R.layout.info_window, null);
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                String title = marker.getTitle();
                TextView txtTitle = (TextView) contents.findViewById(R.id.txtInfoWindowTitle);
                if (title != null) {
                    txtTitle.setText(title);
                    ImageView imageView = (ImageView) contents.findViewById(R.id.ivInfoPicture);
                    if (title.equals("Poznań")) {
                        imageView.setImageResource(R.drawable.ratusz);
                    }
                    if (title.equals("Politechnika Poznańska")) {

                        imageView.setImageResource(R.drawable.pp_cw);
                    }
                } else {
                    txtTitle.setText("");
                }

                String snippet = marker.getSnippet();
                TextView txtSnippet = (TextView) contents.findViewById(R.id.txtInfoWindowSnippet);
                if (snippet != null) {
                    txtSnippet.setText(snippet);
                } else {
                    txtSnippet.setText("");
                }
                return contents;
            }
        });

        PolylineOptions lineOptions = new PolylineOptions()
                .add(new LatLng(52.408513, 16.934110))
                .add(new LatLng(52.408344, 16.934590))
                .add(new LatLng(52.407716, 16.939160))
                .add(new LatLng(52.406943, 16.938667))
                .add(new LatLng(52.403880, 16.942658))
                .add(new LatLng(52.402649, 16.943109))
                .add(new LatLng(52.400882, 16.948258))
                .add(new LatLng(52.403042, 16.950512))
                .add(new LatLng(52.404220, 16.949610))
                .color(Color.RED);
        //map.addPolyline(lineOptions);

        PolygonOptions rectOptions = new PolygonOptions()
                .geodesic(true)
                .add(new LatLng(52.408513, 16.934110))
                .add(new LatLng(52.092668, 16.970955))
                .add(new LatLng(46.942385, -71.249259))
                .add(new LatLng(48.516239, -71.024039))
                .strokeColor(Color.BLUE)
                .fillColor(Color.GRAY);
        //map.addPolygon(rectOptions);


        mGoogleMap = map;
        mMapReady = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);
        //MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        GoogleMapOptions options = new GoogleMapOptions();
        LatLng centerPos = new LatLng(52.406459, 16.941649);
        CameraPosition camPosition = new CameraPosition(centerPos,15,0,0);
        options.mapType(GoogleMap.MAP_TYPE_NORMAL)
                //.liteMode(true)
                .camera(camPosition)
                .compassEnabled(true)
                .rotateGesturesEnabled(true)
                .scrollGesturesEnabled(true)
                .tiltGesturesEnabled(true)
                .zoomControlsEnabled(true)
                .zoomGesturesEnabled(true);

        MapFragment mapFragment = MapFragment.newInstance(options);

        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mapContainer, mapFragment);
        fragmentTransaction.commit();
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_google_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_google_map_normal_type) {
            if (mMapReady)
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            return true;
        }

        if (id == R.id.action_google_map_hybrid_type) {
            if (mMapReady)
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            return true;
        }

        if (id == R.id.action_google_map_satellite_type) {
            if (mMapReady)
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            return true;
        }

        if (id == R.id.action_google_map_terrain_type) {
            if (mMapReady)
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            return true;
        }

        if (id == R.id.action_google_map_none_type) {
            if (mMapReady)
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
            return true;
        }

        if (id == R.id.action_google_map_fly_up) {
            if (mMapReady) {
                LatLng poznan = new LatLng(52.408513, 16.934110);
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(poznan, 15));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 8000, null);
            }
            return true;
        }

        if (id == R.id.action_google_map_fly_far) {
            if (mMapReady) {
                LatLng poznan = new LatLng(52.408513, 16.934110);
                LatLng giewont = new LatLng(49.251190, 19.930016);
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(poznan, 15));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(giewont)
                        .zoom(17)
                        .bearing(90)
                        .tilt(30)
                        .build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),20000,null);
            }
            return true;
        }

        if (id == R.id.action_open_street_map_activity) {
            Intent openStreetMapActivityIntent = new Intent(this,OpenStreetMapActivity.class);
            startActivity(openStreetMapActivityIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        marker.hideInfoWindow();
    }
}
