package com.example.kjankiewicz.android_16w01_mymaps;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer;
import org.osmdroid.bonuspack.kml.KmlDocument;
import org.osmdroid.bonuspack.kml.KmlFeature;
import org.osmdroid.bonuspack.kml.KmlLineString;
import org.osmdroid.bonuspack.kml.KmlPlacemark;
import org.osmdroid.bonuspack.kml.KmlPoint;
import org.osmdroid.bonuspack.kml.KmlPolygon;
import org.osmdroid.bonuspack.kml.Style;
import org.osmdroid.bonuspack.location.NominatimPOIProvider;
import org.osmdroid.bonuspack.location.POI;
import org.osmdroid.bonuspack.overlays.BasicInfoWindow;
import org.osmdroid.bonuspack.overlays.FolderOverlay;
import org.osmdroid.bonuspack.overlays.GroundOverlay;
import org.osmdroid.bonuspack.overlays.InfoWindow;
import org.osmdroid.bonuspack.overlays.MapEventsOverlay;
import org.osmdroid.bonuspack.overlays.MapEventsReceiver;
import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.bonuspack.overlays.Polygon;
import org.osmdroid.bonuspack.overlays.Polyline;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.bonuspack.routing.RoadNode;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

import java.util.ArrayList;

/* kjankiewicz - based on the tutorial at https://code.google.com/p/osmbonuspack/ */

public class OpenStreetMapActivity extends ActionBarActivity implements MapEventsReceiver {

    MapView mMapView;
    GeoPoint mCwPut;
    KmlDocument mKmlDocument;

    class GeoPointArrayList extends ArrayList<GeoPoint>{}

    class GetRoadTask extends AsyncTask<GeoPointArrayList,Void,Road> {

        protected Road doInBackground(GeoPointArrayList... waypoints) {
            OSRMRoadManager osmRoadManager = new OSRMRoadManager();
            return osmRoadManager.getRoad(waypoints[0]);
        }

        protected void onPostExecute(Road road) {
            Polyline roadOverlay = RoadManager.buildRoadOverlay(road, OpenStreetMapActivity.this);
            mMapView.getOverlays().add(roadOverlay);
            mMapView.invalidate();

            Drawable nodeIcon = getResources().getDrawable(R.drawable.route_point);
            for (int i=0; i<road.mNodes.size(); i++){
                RoadNode node = road.mNodes.get(i);
                Marker nodeMarker = new Marker(mMapView);
                nodeMarker.setPosition(node.mLocation);
                nodeMarker.setIcon(nodeIcon);
                nodeMarker.setTitle("Step " + i);
                nodeMarker.setSnippet(node.mInstructions);
                nodeMarker.setSubDescription(Road.getLengthDurationText(node.mLength, node.mDuration));
                Drawable icon;
                if (node.mManeuverType == 1 ) //STRAIGHT see: http://open.mapquestapi.com/guidance/#maneuvertypes
                    icon = getResources().getDrawable(R.drawable.straight);
                else
                    icon = getResources().getDrawable(R.drawable.other_direction);
                nodeMarker.setImage(icon);
                mMapView.getOverlays().add(nodeMarker);
            }
        }
    }

    class GetPOITask extends AsyncTask<GeoPoint,Void,ArrayList<POI>> {

        protected ArrayList<POI> doInBackground(GeoPoint... geoPoint) {
            NominatimPOIProvider poiProvider = new NominatimPOIProvider();
            return poiProvider.getPOICloseTo(geoPoint[0], "cinema", 50, 0.1);
        }

        protected void onPostExecute(ArrayList<POI> pois) {
            if (pois != null) {

                // NonClustered Markers
                /*FolderOverlay poiMarkers = new FolderOverlay(OpenStreetMapActivity.this);

                mMapView.getOverlays().add(poiMarkers);*/

                // Clustered Markers
                RadiusMarkerClusterer poiMarkers = new RadiusMarkerClusterer(OpenStreetMapActivity.this);
                Drawable clusterIconD = getResources().getDrawable(R.drawable.poi_marker_cluster);
                Bitmap clusterIcon = ((BitmapDrawable) clusterIconD).getBitmap();
                poiMarkers.setIcon(clusterIcon);
                poiMarkers.getTextPaint().setColor(Color.BLACK);
                poiMarkers.getTextPaint().setTextSize(20.0f);
                poiMarkers.mAnchorV = Marker.ANCHOR_BOTTOM;
                poiMarkers.mTextAnchorU = 0.58f;
                poiMarkers.mTextAnchorV = 0.68f;

                mMapView.getOverlays().add(poiMarkers);

                mMapView.invalidate();

                Drawable poiIcon = getResources().getDrawable(R.drawable.poi_marker);
                for (POI poi : pois) {
                    Marker poiMarker = new Marker(mMapView);
                    poiMarker.setTitle(poi.mType);
                    poiMarker.setSnippet(poi.mDescription);
                    poiMarker.setPosition(poi.mLocation);
                    poiMarker.setIcon(poiIcon);
                    if (poi.mThumbnail != null) {
                        poiMarker.setImage(new BitmapDrawable(
                                getApplicationContext().getResources(),
                                poi.mThumbnail));
                    }
                    poiMarker.setRelatedObject(poi);

                    poiMarkers.add(poiMarker);
                }
            }
        }
    }

    class MyKmlStyler implements KmlFeature.Styler {
        Style mDefaultStyle;

        Style mEvenStyle;
        Style mOddStyle;

        MyKmlStyler(Style defaultStyle){
            Drawable defaultMarker = getResources().getDrawable(R.drawable.kml_small);
            Bitmap defaultBitmap = ((BitmapDrawable)defaultMarker).getBitmap();
            mDefaultStyle = defaultStyle;
            mEvenStyle = new Style(defaultBitmap, Color.argb(255,255,140,0), 3.0f, Color.argb(255,255,140,0));
            mKmlDocument.putStyle("EvenStyle",mEvenStyle);
            mOddStyle = new Style(defaultBitmap, Color.RED, 3.0f, Color.RED);
            mKmlDocument.putStyle("OddStyle", mOddStyle);
        }

        @Override public void onLineString(Polyline polyline, KmlPlacemark kmlPlacemark, KmlLineString kmlLineString) {
            //Custom styling:
            String title = polyline.getTitle();
            if (title.endsWith("0") || title.endsWith("2") ||
                    title.endsWith("4") || title.endsWith("6") ||
                    title.endsWith("8")) {
                kmlPlacemark.mStyle = "EvenStyle";
                kmlLineString.applyDefaultStyling(polyline, mDefaultStyle, kmlPlacemark, mKmlDocument, mMapView);
            } else {
                kmlPlacemark.mStyle = "OddStyle";
                kmlLineString.applyDefaultStyling(polyline, mDefaultStyle, kmlPlacemark, mKmlDocument, mMapView);
            }
            polyline.setWidth(Math.max(kmlLineString.mCoordinates.size()/200.0f, 3.0f));
        }
        @Override public void onPolygon(Polygon polygon, KmlPlacemark kmlPlacemark, KmlPolygon kmlPolygon) {
            //Keeping default styling:
            kmlPolygon.applyDefaultStyling(polygon, mDefaultStyle, kmlPlacemark, mKmlDocument, mMapView);
        }
        @Override public void onPoint(Marker marker, KmlPlacemark kmlPlacemark, KmlPoint kmlPoint) {
            //Keeping default styling:
            kmlPoint.applyDefaultStyling(marker, mDefaultStyle, kmlPlacemark, mKmlDocument, mMapView);
        }
        @Override public void onFeature(Overlay overlay, KmlFeature kmlFeature) {
            //If nothing to do, do nothing.
        }
    }

    class GetKML extends AsyncTask<String,Void,KmlDocument> {

        protected KmlDocument doInBackground(String... urls) {
            mKmlDocument = new KmlDocument();
            mKmlDocument.parseKMLUrl(urls[0]);
            return mKmlDocument;
        }

        protected void onPostExecute(KmlDocument kmlDocument) {

            // DefaultStyle
            //FolderOverlay kmlOverlay = (FolderOverlay)kmlDocument.mKmlRoot.buildOverlay(mMapView, null, null, kmlDocument);

            // DefinedStyle
            Drawable defaultMarker = getResources().getDrawable(R.drawable.kml_small);
            Bitmap defaultBitmap = ((BitmapDrawable)defaultMarker).getBitmap();
            Style defaultStyle = new Style(defaultBitmap, 0xD65CADFF, 4.0f, 0xE699CCFF);
            KmlFeature.Styler styler = new MyKmlStyler(defaultStyle);
            FolderOverlay kmlOverlay = (FolderOverlay)kmlDocument.mKmlRoot.buildOverlay(mMapView, defaultStyle, styler, kmlDocument);

            mMapView.getOverlays().add(kmlOverlay);
            mMapView.invalidate();
            mMapView.zoomToBoundingBox(kmlDocument.mKmlRoot.getBoundingBox());
        }
    }

    //

    // 13 Styling overlays
            /*Drawable defaultMarker = getResources().getDrawable(R.drawable.marker_kml_point);
            Bitmap defaultBitmap = ((BitmapDrawable)defaultMarker).getBitmap();
            Style defaultStyle = new Style(defaultBitmap, 0x901010AA, 3.0f, 0x20AA1010);
            KmlFeature.Styler styler = new MyKmlStyler(defaultStyle);
            FolderOverlay kmlOverlay = (FolderOverlay)kmlDocument.mKmlRoot.buildOverlay(mMapView, defaultStyle, styler, kmlDocument);
            */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_street_map);
        mMapView = (MapView) findViewById(R.id.map);
        mMapView.setTileSource(TileSourceFactory.MAPNIK);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);

        mCwPut = new GeoPoint(52.404220, 16.949610);
        IMapController mapController = mMapView.getController();
        mapController.setZoom(14);
        //mapController.setCenter(mCwPut);
        mMapView.invalidate();

        new Handler(Looper.getMainLooper()).postDelayed(
                new Runnable() {
                    public void run() {
                        mMapView.getController().setCenter(mCwPut);
                    }
                }, 1000
        );

        Marker cwPutMarker = new Marker(mMapView);
        cwPutMarker.setDraggable(true);
        cwPutMarker.setPosition(mCwPut);
        cwPutMarker.setTitle("Politechnika Poznańska");
        cwPutMarker.setSnippet("Centrum Wykładowe");
        cwPutMarker.setImage(getResources().getDrawable(R.drawable.pp_cw));
        cwPutMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        mMapView.getOverlays().add(cwPutMarker);
        mMapView.invalidate();

        //cwPutMarker.setIcon(getResources().getDrawable(R.drawable.pp));

        GeoPointArrayList waypoints = new GeoPointArrayList();
        waypoints.add(mCwPut);
        GeoPoint sremMarket = new GeoPoint(52.094845, 17.021266);
        waypoints.add(sremMarket);

        new GetRoadTask().execute(waypoints);

        new GetPOITask().execute(mCwPut);

        new GetKML().execute("http://mtk2.toursprung.com/export/outdoorish_bikemap_paths/2372927.kml?60559812");

        MapEventsOverlay mapEventsOverlay = new MapEventsOverlay(this, this);
        mMapView.getOverlays().add(0, mapEventsOverlay);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_open_street_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_osm_mapnik_type) {
            mMapView.setTileSource(TileSourceFactory.MAPNIK);
            return true;
        }

        if (id == R.id.action_osm_cyclemap_type) {
            mMapView.setTileSource(TileSourceFactory.CYCLEMAP);
            return true;
        }

        if (id == R.id.action_osm_osmpublictransport_type) {
            mMapView.setTileSource(TileSourceFactory.PUBLIC_TRANSPORT);
            return true;
        }

        if (id == R.id.action_osm_mapquestosm_type) {
            mMapView.setTileSource(TileSourceFactory.MAPQUESTOSM);
            return true;
        }

        if (id == R.id.action_osm_mapquestaerial_type) {
            mMapView.setTileSource(TileSourceFactory.MAPQUESTAERIAL);
            return true;
        }

        if (id == R.id.action_osm_cloudstandard_type) {
            mMapView.setTileSource(TileSourceFactory.CLOUDMADESTANDARDTILES);
            return true;
        }

        if (id == R.id.action_osm_cloudsmall_type) {
            mMapView.setTileSource(TileSourceFactory.CLOUDMADESMALLTILES);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean singleTapConfirmedHelper(GeoPoint p) {
        Toast.makeText(this, "We are at " + p.getLatitude() + "," + p.getLongitude(), Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public boolean longPressHelper(GeoPoint p) {
        InfoWindow.closeAllInfoWindowsOn(mMapView);

        Polygon circle = new Polygon(this);
        circle.setPoints(Polygon.pointsAsCircle(p, 1000.0));

        circle.setFillColor(0x12121212);
        circle.setStrokeColor(0xD65CADFF);
        circle.setStrokeWidth(2);

        mMapView.getOverlays().add(circle);

        circle.setInfoWindow(new BasicInfoWindow(R.layout.bonuspack_bubble, mMapView));
        circle.setTitle("Circle on "+p.getLatitude()+","+p.getLongitude());

        GroundOverlay myGroundOverlay = new GroundOverlay(this);
        myGroundOverlay.setPosition(p);
        myGroundOverlay.setImage(getResources().getDrawable(R.drawable.kml_small).mutate());
        myGroundOverlay.setDimensions(1000.0f);
        mMapView.getOverlays().add(myGroundOverlay);

        mMapView.invalidate();

        return true;
    }

    /*Polygon circle = new Polygon(this);
    circle.setPoints(Polygon.pointsAsCircle(p, 2000.0));
    circle.setFillColor(0x12121212);
    circle.setStrokeColor(Color.RED);
    circle.setStrokeWidth(2);
    mMapView.getOverlays().add(circle);

    circle.setInfoWindow(new BasicInfoWindow(R.layout.bonuspack_bubble, mMapView));
    circle.setTitle("Centered on "+p.getLatitude()+","+p.getLongitude());

    GroundOverlay myGroundOverlay = new GroundOverlay(this);
    myGroundOverlay.setPosition(p);
    myGroundOverlay.setImage(getResources().getDrawable(R.drawable.ic_launcher).mutate());
    myGroundOverlay.setDimensions(2000.0f);
    mMapView.getOverlays().add(myGroundOverlay);

    mMapView.invalidate();*/
}
